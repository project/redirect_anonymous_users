<?php

namespace Drupal\redirect_anonymous_users\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Redirect anonymous users settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'redirect_anonymous_users_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['redirect_anonymous_users.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['routes_to_exclude'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Routes to Exclude'),
      '#description' => $this->t('Enter the list of routes names to exclude from redirection (one per line).'),
      '#default_value' => $this->config('redirect_anonymous_users.settings')->get('routes_to_exclude'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $routesToExcludeValue = $form_state->getValue('routes_to_exclude');
    if (str_contains($routesToExcludeValue, '/')) {
      $form_state->setErrorByName('routes_to_exclude', $this->t('Only include route names. No paths accepted.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $excludedRoutesValue = $form_state->getValue('routes_to_exclude');
    $excludedRoutesSplit = preg_split('/\s+/', $excludedRoutesValue, -1, PREG_SPLIT_NO_EMPTY);

    $this->config('redirect_anonymous_users.settings')
      ->set('routes_to_exclude', $excludedRoutesValue)
      ->set('routes_to_exclude_split', $excludedRoutesSplit)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
