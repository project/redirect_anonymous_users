<?php

namespace Drupal\redirect_anonymous_users\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class RedirectAnonymousSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The settings for this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $configSettings;

  /**
   * Constructor for RedirectAnonymousSubscriber.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   THe config factory.
   */
  public function __construct(AccountProxyInterface $currentUser, RouteMatchInterface $routeMatch, ConfigFactoryInterface $configFactory) {
    $this->currentUser = $currentUser;
    $this->routeMatch = $routeMatch;
    $this->configSettings = $configFactory->get('redirect_anonymous_users.settings');
  }

  /**
   * Function to check authentication status.
   */
  public function checkAuthStatus() {
    $excludedRoutes = $this->configSettings->get('routes_to_exclude_split') ?? [];

    if (
      $this->currentUser->isAnonymous() &&
      $this->routeMatch->getRouteName() != 'user.login' &&
      !in_array($this->routeMatch->getRouteName(), $excludedRoutes)
    ) {
      $redirectPath = Url::fromRoute('user.login')->toString();
      $response = new RedirectResponse($redirectPath, 302);
      $response->send();
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['checkAuthStatus'];

    return $events;
  }

}
